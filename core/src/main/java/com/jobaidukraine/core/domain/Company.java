package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Company {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;

  private String name;
  private String url;
  private String email;
  private String logo;

  private Set<Job> jobs;
  private Address address;
  private User contactUser;
}
