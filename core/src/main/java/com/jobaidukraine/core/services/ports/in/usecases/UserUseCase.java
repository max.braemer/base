package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.User;

public interface UserUseCase {
  User save(User user);

  User update(User user);

  void delete(long id);
}
