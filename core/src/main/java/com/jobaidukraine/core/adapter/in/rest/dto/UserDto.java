package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
  @Schema(
      description =
          "The database generated user ID. "
              + "This hast to be null when creating a new user. "
              + "This has to be set when updating an existing user.",
      example = "1")
  private Long id;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated creation date of the language level. "
              + "This has to be null when creating a new language level.",
      example = "2020-01-01T00:00:00.000Z")
  private LocalDateTime createdAt;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated last update date of the job. "
              + "This has to be null when creating a new job.",
      example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime updatedAt;

  @Schema(
      description =
          "The database generated version of the language level. "
              + "This has to be null when creating a new language level.",
      example = "1")
  private Integer version;

  private Boolean deleted;

  @Email(message = "The user email is not valid.")
  @NotNull(message = "The user email may not be null.")
  @NotBlank(message = "The user email may not be blank or empty.")
  @Schema(description = "The user email.", example = "user@gmail.com")
  private String email;

  @NotNull(message = "The user firstname may not be null.")
  @NotEmpty(message = "The user firstname may not be empty.")
  @Schema(description = "The user firstname.", example = "John")
  private String firstname;

  @NotNull(message = "The user lastname may not be null.")
  @NotEmpty(message = "The user lastname may not be empty.")
  @Schema(description = "The user lastname.", example = "Doe")
  private String lastname;

  @Schema(description = "The user position.", example = "Developer")
  private String position;

  @Schema(description = "The user country.", example = "France")
  private String country;

  @NotNull(message = "The role may not be null.")
  @NotBlank(message = "The role may not be blank or empty.")
  @Schema(description = "The user role.", example = "ROLE_USER")
  private RoleDto role;

  @Schema(description = "The user experience.", example = "PROFESSIONAL")
  private ExperienceDto experience;

  @Valid
  @Schema(description = "The user language skills")
  private Set<LanguageLevelDto> languageLevels;
}
