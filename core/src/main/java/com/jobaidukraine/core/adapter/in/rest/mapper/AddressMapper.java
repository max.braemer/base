package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.AddressDto;
import com.jobaidukraine.core.domain.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "InRestAddressMapperImpl")
public interface AddressMapper {
  Address dtoToDomain(AddressDto addressDto);

  AddressDto domainToDto(Address address);
}
