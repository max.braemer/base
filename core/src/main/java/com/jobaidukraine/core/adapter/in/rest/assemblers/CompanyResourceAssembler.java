package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.CompanyController;
import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CompanyResourceAssembler
    implements RepresentationModelAssembler<CompanyDto, EntityModel<CompanyDto>> {

  @Override
  public EntityModel<CompanyDto> toModel(CompanyDto company) {

    try {
      return EntityModel.of(
          company,
          WebMvcLinkBuilder.linkTo(methodOn(CompanyController.class).show(company.getId()))
              .withSelfRel(),
          linkTo(methodOn(CompanyController.class).list(null)).withRel("companies"));
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
