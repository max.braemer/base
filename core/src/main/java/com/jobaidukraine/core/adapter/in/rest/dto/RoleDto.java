package com.jobaidukraine.core.adapter.in.rest.dto;

public enum RoleDto {
  ADMIN,
  EDITOR,
  USER,
}
