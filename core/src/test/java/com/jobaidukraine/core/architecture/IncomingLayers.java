package com.jobaidukraine.core.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import java.util.ArrayList;
import java.util.List;

public class IncomingLayers extends ArchitectureElement {

  private final HexagonalArchitecture parentContext;
  private final List<String> incomingLayerPackages = new ArrayList<>();

  IncomingLayers(HexagonalArchitecture parentContext, String basePackage) {
    super(basePackage);
    this.parentContext = parentContext;
  }

  public IncomingLayers withPackages(String packageName) {
    this.incomingLayerPackages.add(fullQualifiedPackage(packageName));
    return this;
  }

  public HexagonalArchitecture and() {
    return parentContext;
  }

  String getBasePackage() {
    return basePackage;
  }

  void dontDependOnEachOther(JavaClasses classes) {
    List<String> allIncomingLayers = incomingLayerPackages;
    for (String adapter1 : allIncomingLayers) {
      for (String adapter2 : allIncomingLayers) {
        if (!adapter1.equals(adapter2)) {
          denyDependency(adapter1, adapter2, classes);
        }
      }
    }
  }

  void doesNotDependOn(String packageName, JavaClasses classes) {
    incomingLayerPackages.forEach(incoming -> denyDependency(incoming, packageName, classes));
  }

  void doesNotContainEmptyPackages() {
    denyEmptyPackages(incomingLayerPackages);
  }
}
