package com.jobaidukraine.core.adapter.out.db.implementation.company;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.adapter.out.db.entities.AddressEntity;
import com.jobaidukraine.core.adapter.out.db.entities.CompanyEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.UserEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.CompanyMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.CompanyRepository;
import com.jobaidukraine.core.domain.Address;
import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.User;
import java.time.LocalDateTime;
import java.util.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class CompanyAdapterTest {

  @Mock CompanyRepository companyRepository;
  @Mock CompanyMapper companyMapper;
  @InjectMocks CompanyAdapter companyAdapter;

  @Test
  void findExistingCompanyById() {
    CompanyEntity companyEntity = getCompanyEntityWithId(1L);
    Company company = getCompanyWithId(1L);

    when(companyRepository.findById(1L)).thenReturn(Optional.of(companyEntity));
    when(companyMapper.toDomain(argThat(new CompanyEntityMatcher(companyEntity))))
        .thenReturn(company);

    Optional<Company> result = companyAdapter.findById(1L);

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(company);
  }

  @Test
  void findExistingCompaniesAllByPageable() {
    CompanyEntity companyEntity1 = getCompanyEntityWithId(1L);
    CompanyEntity companyEntity2 = getCompanyEntityWithId(2L);
    Company company1 = getCompanyWithId(1L);
    Company company2 = getCompanyWithId(2L);
    Page<CompanyEntity> companyEntityPage = new PageImpl<>(List.of(companyEntity1, companyEntity2));

    when(companyRepository.findAll(PageRequest.of(0, 10))).thenReturn(companyEntityPage);
    when(companyMapper.toDomain(argThat(new CompanyEntityMatcher(companyEntity1))))
        .thenReturn(company1);
    when(companyMapper.toDomain(argThat(new CompanyEntityMatcher(companyEntity2))))
        .thenReturn(company2);

    Page<Company> result = companyAdapter.findAllByPageable(PageRequest.of(0, 10));

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(company1, company2));
  }

  @Test
  void saveCompany() {
    Company company = getCompanyWithId(1L);
    Company companyNull = getCompanyWithId(null);
    CompanyEntity companyEntity = getCompanyEntityWithId(1L);
    CompanyEntity companyEntityNull = getCompanyEntityWithId(null);

    when(companyMapper.toEntity(argThat(new CompanyMatcher(companyNull))))
        .thenReturn(companyEntityNull);
    when(companyRepository.save(argThat(new CompanyEntityMatcher(companyEntityNull))))
        .thenReturn(companyEntity);
    when(companyMapper.toDomain(argThat(new CompanyEntityMatcher(companyEntity))))
        .thenReturn(company);

    Company result = companyAdapter.save(companyNull);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(company);
  }

  @Test
  void updateExistingCompany() {
    CompanyEntity companyEntity = getCompanyEntityWithId(1L);
    Company company = getCompanyWithId(1L);

    when(companyRepository.findById(1L)).thenReturn(Optional.of((companyEntity)));
    when(companyRepository.save(argThat(new CompanyEntityMatcher(companyEntity))))
        .thenReturn(companyEntity);
    when(companyMapper.toDomain(argThat(new CompanyEntityMatcher(companyEntity))))
        .thenReturn(company);

    Company result = companyAdapter.update(company);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(company);
  }

  @Test
  void updateCompanyWithMostAttributesNull() {
    Company company = getCompanyWithId(1L);
    CompanyEntity companyEntity = getCompanyEntityWithId(1L);

    when(companyRepository.findById(1L)).thenReturn(Optional.of((companyEntity)));
    when(companyRepository.save(argThat(new CompanyEntityMatcher(companyEntity))))
        .thenReturn(companyEntity);
    when(companyMapper.toDomain(argThat(new CompanyEntityMatcher(companyEntity))))
        .thenReturn(company);

    Company result =
        companyAdapter.update(
            new Company(1L, null, null, null, null, null, null, null, null, null, null, null));

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(companyEntity);
  }

  @Test
  void deleteExistingCompany() {
    CompanyEntity companyEntity = getCompanyEntityWithId(1L);

    when(companyRepository.findById(1L)).thenReturn(Optional.of((companyEntity)));

    companyAdapter.delete(1L);

    verify(companyRepository).save(argThat(new CompanyEntityMatcher(companyEntity)));
  }

  CompanyEntity getCompanyEntityWithId(Long companyId) {
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<JobEntity> jobs = new HashSet<>(Collections.emptySet());
    AddressEntity address = new AddressEntity();
    UserEntity contactUser = new UserEntity();

    return new CompanyEntity(
        companyId,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }

  Company getCompanyWithId(Long companyId) {
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<Job> jobs = new HashSet<>(Collections.emptySet());
    Address address = new Address();
    User contactUser = new User();
    return new Company(
        companyId,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }

  private static class CompanyMatcher implements ArgumentMatcher<Company> {
    private final Company expected;

    public CompanyMatcher(Company expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(Company actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getId(), expected.getId())
          && Objects.equals(actual.getCreatedAt(), expected.getCreatedAt())
          && Objects.equals(actual.getUpdatedAt(), expected.getUpdatedAt())
          && Objects.equals(actual.getVersion(), expected.getVersion())
          && Objects.equals(actual.getDeleted(), expected.getDeleted())
          && Objects.equals(actual.getName(), expected.getName())
          && Objects.equals(actual.getUrl(), expected.getUrl())
          && Objects.equals(actual.getEmail(), expected.getEmail())
          && Objects.equals(actual.getLogo(), expected.getLogo())
          && Objects.equals(actual.getJobs(), expected.getJobs())
          && Objects.equals(actual.getAddress(), expected.getAddress())
          && Objects.equals(actual.getContactUser(), expected.getContactUser());
    }
  }

  private static class CompanyEntityMatcher implements ArgumentMatcher<CompanyEntity> {
    private final CompanyEntity expected;

    public CompanyEntityMatcher(CompanyEntity expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(CompanyEntity actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getId(), expected.getId())
          && Objects.equals(actual.getCreatedAt(), expected.getCreatedAt())
          && Objects.equals(actual.getUpdatedAt(), expected.getUpdatedAt())
          && Objects.equals(actual.getVersion(), expected.getVersion())
          && Objects.equals(actual.getDeleted(), expected.getDeleted())
          && Objects.equals(actual.getName(), expected.getName())
          && Objects.equals(actual.getUrl(), expected.getUrl())
          && Objects.equals(actual.getEmail(), expected.getEmail())
          && Objects.equals(actual.getLogo(), expected.getLogo())
          && Objects.equals(actual.getJobs(), expected.getJobs())
          && Objects.equals(actual.getAddress(), expected.getAddress())
          && Objects.equals(actual.getContactUser(), expected.getContactUser());
    }
  }
}
