package com.jobaidukraine.core.services.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.domain.Experience;
import com.jobaidukraine.core.domain.LanguageLevel;
import com.jobaidukraine.core.domain.Role;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
  @Mock private UserPort userPort;

  @InjectMocks private UserService userService;

  @Test
  void saveUser() {
    User user = getMockUser();

    when(userPort.save(user)).thenReturn(user);

    User result = userService.save(user);
    assertEquals(result, user);
  }

  @Test
  void findAllByPageable() {
    Page<User> user = new PageImpl<>(List.of(getMockUser()));

    when(userPort.findAllByPageable(Pageable.ofSize(1))).thenReturn(user);

    Page<User> result = userService.findAllByPageable(PageRequest.of(0, 1));

    assertEquals(result, user);
  }

  @Test
  void findById() {
    User user = getMockUser();

    when(userPort.findById(1L)).thenReturn(Optional.of(user));

    User result = userService.findById(1L);
    assertEquals(result, user);
  }

  @Test
  @DisplayName("no user for id 2L found")
  void findById2() {
    when(userPort.findById(2L)).thenThrow(new NoSuchElementException());

    assertThrows(NoSuchElementException.class, () -> userService.findById(2L));
  }

  @Test
  void findByEmail() {
    Optional<User> user = Optional.of(getMockUser());

    when(userPort.findByEmail("test@test.test")).thenReturn(user);

    Optional<User> result = userService.findByEmail("test@test.test");
    assertEquals(result, user);
  }

  @Test
  void update() {
    User base = getMockUser();
    User update = getMockUser();
    update.setCountry("germany");

    when(userPort.update(base)).thenReturn(update);

    User result = userService.update(base);
    assertEquals(result, update);
  }

  @Test
  void delete() {
    User user = getMockUser();
    userService.delete(user.getId());
    verify(userPort).delete(user.getId());
  }

  User getMockUser() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String email = "test@test.test";
    String firstname = "test firstname";
    String lastname = "test lastname";
    String position = "test position";
    String country = "test country";
    Role role = Role.USER;
    Experience experience = Experience.NEW_GRADUATE;
    Set<LanguageLevel> languageLevels = new HashSet<>();
    return new User(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        email,
        firstname,
        lastname,
        position,
        country,
        role,
        experience,
        languageLevels);
  }
}
