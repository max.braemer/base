package com.jobaidukraine.core.services.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.domain.*;
import com.jobaidukraine.core.services.ports.out.JobPort;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class JobServiceTest {

  @Mock private JobPort jobPort;

  @InjectMocks private JobService jobService;

  @Test
  void saveJob() {
    Job job = getMockJob();
    when(jobPort.save(job)).thenReturn(job);

    Job result = jobService.save(job);

    assertEquals(result, job);
  }

  @Test
  @DisplayName("findAllByPageable without specific title")
  void findAllByPageable() {
    Page<Job> job = new PageImpl<>(List.of(getMockJob()));
    when(jobPort.findAllByPageable(Pageable.ofSize(1))).thenReturn(job);

    Page<Job> result = jobService.findAllByPageable(PageRequest.of(0, 1));

    assertEquals(result, job);
  }

  @Test
  @DisplayName("findAllByPageable with specific title")
  void findAllByPageable2() {
    Page<Job> job = new PageImpl<>(List.of(getMockJob()));
    when(jobPort.findAllByPageable(Pageable.ofSize(1), "test title")).thenReturn(job);

    Page<Job> result = jobService.findAllByPageable(PageRequest.of(0, 1), "test title");

    assertEquals(result, job);
  }

  @Test
  void findById() {
    Job job = getMockJob();
    when(jobPort.findById(1L)).thenReturn(Optional.of(job));

    Job result = jobService.findById(1L);

    assertEquals(result, job);
  }

  @Test
  @DisplayName("no job for id 2L found")
  void findById2() {
    when(jobPort.findById(2L)).thenThrow(new NoSuchElementException());

    assertThrows(NoSuchElementException.class, () -> jobService.findById(2L));
  }

  @Test
  void update() {
    Job base = getMockJob();
    Job update = getMockJob();
    update.setTitle("Test2");
    when(jobPort.update(base)).thenReturn(update);

    Job result = jobService.update(base);

    assertEquals(result, update);
  }

  @Test
  void delete() {
    Job job = getMockJob();

    jobService.delete(job.getId());

    verify(jobPort).delete(job.getId());
  }

  Job getMockJob() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    JobType jobType = JobType.FREELANCE;
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    Address address = new Address();
    Set<Experience> qualifications = new HashSet<>();
    Set<LanguageLevel> languageLevels = new HashSet<>();
    return new Job(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        null,
        languageLevels);
  }
}
