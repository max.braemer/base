
export default function useDate(date: Date, format: String = ''): String {
    return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes()
}